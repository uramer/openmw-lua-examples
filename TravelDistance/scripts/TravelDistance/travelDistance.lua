local self = require('openmw.self')
local ui = require('openmw.ui')
local util = require('openmw.util')
local input = require('openmw.input')
local I = require('openmw.interfaces')

local v2 = util.vector2

local position = nil
local distance = 0
local recording = false
local savedPositions = {}

local function renderDistance(d)
  return string.format("%.2f", d / 22.1) .. ' feet'
end

local distanceText = {
  template = I.MWUI.templates.textNormal,
  props = {
    relativePosition = v2(0.5, 0.5),
    anchor = v2(0.5, 0.5),
    text = renderDistance(0),
  },
}

local menuLayout = {
  layer = 'HUD',
  template = I.MWUI.templates.boxTransparent,
  content = ui.content {
    {
      type = ui.TYPE.Flex,
      content = ui.content {
        {
          template = I.MWUI.templates.textHeader,
          props = {
            text = 'Travel distance',
          },
        },
        distanceText
      },
    },
  },
}

local savedPositionsLayout = {
  type = ui.TYPE.Flex,
  content = ui.content {}
}

local savedLayout = {
  layer = 'Windows',
  template = I.MWUI.templates.boxTransparent,
  props = {
    relativePosition = v2(0.5, 0.5),
    anchor = v2(0.5, 0.5),
    text = 'Saved positions',
  },
  content = ui.content {
    {
      type = ui.TYPE.Flex,
      content = ui.content {
        {
          template = I.MWUI.templates.textHeader,
          props = {
            text = 'Travel distance',
          },
        },
        savedPositionsLayout,
      }
    }
  }
}

local function renderPositions(list)
  local result = {}
  for i, it in ipairs(list) do
    result[i] = {
      template = I.MWUI.templates.textNormal,
      props = {
        text = table.concat{ 'distance: ', renderDistance(it.distance), '; position: ', tostring(it.position) }
      },
    }
  end
  return result
end

local menu = nil
local savedList = nil

local function toggle()
  if menu then
    menu:destroy()
    recording = false
    menu = nil
    if #savedPositions > 0 then
      savedPositionsLayout.content = ui.content(renderPositions(savedPositions))
      savedList = ui.create(savedLayout)
    end
  elseif savedList then
    savedList:destroy()
    savedList = nil
  else
    distance = 0
    position = nil
    savedPositions = {}
    recording = true
    menu = ui.create(menuLayout)
  end
end

local function savePosition()
  if not recording or not position then return end
  savedPositions[#savedPositions + 1] = {
    position = position,
    distance = distance,
  }
  ui.showMessage('Saved current position')
end

local function updateDistance()
  if not recording then return end
  local newPosition = self.object.position
  position = position or newPosition
  distance = distance + (newPosition - position):length()
  position = newPosition
  distanceText.props.text = table.concat{ renderDistance(distance) }
  menu:update()
end

return {
  engineHandlers = {
    onKeyPress = function(key)
      if key.code == input.KEY.Comma then
        toggle()
      elseif key.code == input.KEY.Slash then
        savePosition()
      end
    end,
    onUpdate = function(_)
      updateDistance()
    end,
  }
}