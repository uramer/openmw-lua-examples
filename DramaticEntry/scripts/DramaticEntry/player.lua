local self = require('openmw.self')
local camera = require('openmw.camera')
local util = require('openmw.util')
local input = require('openmw.input')
local ui = require('openmw.ui')

local time = require('openmw_aux.time')

local events = require('scripts.DramaticEntry.events')
local bezier = require('scripts.DramaticEntry.bezier')
local bezier4, bezier5 = bezier.cubic, bezier.quadratic

local ZOOM_DURATION = 3 * time.second
local ORBIT_DURATION = 2 * time.second
local ORBIT_ANGLE = math.pi
local ORBIT_SHIFT = 20
local RETURN_DURATION = 3 * time.second
local RETURN_SHIFT = 100

local HEIGHT_OFFSET = util.vector3(0, 0, 123)
local HEAD_DISTANCE = 30
local CAMERA_ANCHOR_DISTANCE = 100
local CAMERA_MERGE_DISTANCE = 10

local PICK_DISTANCE = 1000
local PICK_PERIOD = 0.5 * time.second

local BLACK_BAR_SIZE = 0.15

if ui.layers.indexOf('DramaticEntry') == nil then
    ui.layers.insertAfter('Pointer', 'urm_DramaticEntry', {
        interactive = false,
    })
end

local blackBars = ui.create {
    props = {
        relativeSize = util.vector2(1, 1),
    },
    content = ui.content {
        {
            template = { skin = 'FullBlackBG' },
            props = {
                relativeSize = util.vector2(1, BLACK_BAR_SIZE),
            },
        },
        {
            template = { skin = 'FullBlackBG' },
            props = {
                relativePosition = util.vector2(0, 1),
                relativeSize = util.vector2(1, BLACK_BAR_SIZE),
                anchor = util.vector2(0, 1),
            },
        },
    },
}
local function showBars(flag)
    blackBars.layout.layer = flag and 'urm_DramaticEntry' or nil
    blackBars:update()
end

local ALL_CONTROLS = {
    'Controls',
    'Fighting',
    'Jumping',
    'Looking',
    'Magic',
    'VanityMode',
    'ViewMode',
}
local function enableControls(flag)
    for _, v in pairs(ALL_CONTROLS) do
        input.setControlSwitch(input.CONTROL_SWITCH[v], flag)
    end
end

local function rotationFromDirection(direction)
    local direction, length = direction:normalize()
    if length < 0 then
        direction = -direction
    end
    return util.vector2(
        math.atan2(direction.x, direction.y),
        math.asin(direction.z)
    )
end

local function actorRotation(actor)
    return util.vector2(actor.rotation.z, actor.rotation.x)
end

local function directionFromRotation(rotation)
    return util.transform.rotateZ(-rotation.x)
        * util.transform.rotateX(-rotation.y)
        * util.vector3(0, 1, 0)
end

local function applyCurve(curve, duration)
    local t = 0
    local cameraPosition, cameraDirection
    while t < duration do
        cameraPosition, cameraDirection = curve(t / duration)
        camera.setStaticPosition(cameraPosition)

        local rotation = rotationFromDirection(cameraDirection)
        camera.setYaw(rotation.x)
        camera.setPitch(rotation.y)

        t = t + coroutine.yield()
    end
    return cameraPosition, cameraDirection
end

local function animate(actor)
    actor:sendEvent(events.animateStart)
    local previousCameraMode = camera.getMode()
    camera.setMode(camera.MODE.Static)
    enableControls(false)
    showBars(true)

    local actorHeadPosition = actor.position + HEIGHT_OFFSET
    local originalCameraPosition = camera.getPosition()
    local originalCameraDirection = directionFromRotation(
        actorRotation(self) + util.vector2(camera.getYaw(), camera.getPitch())
    )

    local directionToActor = (actorHeadPosition - originalCameraPosition):normalize()
    local zoomTrajectory = bezier4 {
        originalCameraPosition,
        originalCameraPosition + originalCameraDirection * CAMERA_ANCHOR_DISTANCE,
        actorHeadPosition - directionToActor * CAMERA_ANCHOR_DISTANCE,
        actorHeadPosition - directionToActor * HEAD_DISTANCE,
    }
    local function zoomCurve(t)
        return zoomTrajectory(1 - (1 - t) * (1 - t));
    end
    local zoomEndPosition = applyCurve(zoomCurve, ZOOM_DURATION)

    local orbitStartDirection, orbitDistance = (actorHeadPosition - zoomEndPosition):normalize()
    local function orbitTrajectory(t)
        local cameraDirection = util.transform.rotateZ(ORBIT_ANGLE * t) * orbitStartDirection
        local shift = util.transform.rotateZ(math.pi * 0.5) * cameraDirection * ORBIT_SHIFT
        local cameraPosition = actorHeadPosition - cameraDirection * orbitDistance + shift * t
        return cameraPosition, cameraDirection
    end
    local function orbitCurve(t)
        return orbitTrajectory((1 - math.cos(math.pi * t)) * 0.5)
    end
    local orbitEndPosition, orbitEndDirection = applyCurve(orbitCurve, ORBIT_DURATION)

    local mergeDistance = previousCameraMode == camera.MODE.FirstPerson and CAMERA_MERGE_DISTANCE or 0
    local returnShift = directionToActor * RETURN_SHIFT
    local returnTrajectory = bezier5 {
        orbitEndPosition,
        orbitEndPosition + orbitEndDirection * CAMERA_ANCHOR_DISTANCE,
        originalCameraPosition + returnShift,
        originalCameraPosition - originalCameraDirection * CAMERA_ANCHOR_DISTANCE,
        originalCameraPosition - originalCameraDirection * mergeDistance,
    }
    local function returnCurve(t)
        return returnTrajectory((1 - math.cos(math.pi * t)) * 0.5);
    end
    applyCurve(returnCurve, RETURN_DURATION)

    showBars(false)
    enableControls(true)
    camera.setMode(previousCameraMode)
    actor:sendEvent(events.animateEnd)
    return actor
end

local currentAnimation = nil
local notableActors = {}
local doneActors = {}

local function pickEntryActor()
    if currentAnimation then return end
    local minActor, minDistanceSq = nil, PICK_DISTANCE * PICK_DISTANCE
    for _, actor in pairs(notableActors) do
        local distanceSq = (actor.position - self.position):length2()
        if distanceSq < minDistanceSq then
            minActor, minDistanceSq = actor, distanceSq
        end
    end
    if minActor then
        currentAnimation = coroutine.wrap(animate)
        currentAnimation(minActor)
    end
end

time.runRepeatedly(pickEntryActor, PICK_PERIOD)

return {
    engineHandlers = {
        onLoad = function(data)
            if data and data.done then
                doneActors = data.done
            end
        end,
        onSave = function()
            return { done = doneActors }
        end,
        onUpdate = function(dt)
            if not currentAnimation then return end
            local doneActor = currentAnimation(dt)
            if doneActor then
                currentAnimation = nil
                doneActors[doneActor.recordId] = true
                notableActors[doneActor.recordId] = nil
            end
        end,
    },
    eventHandlers = {
        [events.active] = function(event)
            if not doneActors[event.actor.recordId] then
                notableActors[event.actor.recordId] = event.actor
            end
        end,
        [events.inactive] = function(event)
            notableActors[event.actor.recordId] = nil
        end,
    },
}