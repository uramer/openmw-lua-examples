local world = require('openmw.world')
local core = require('openmw.core')
local types = require('openmw.types')

local events = require('scripts.DramaticEntry.events')

local actorList = {
  'caius cosades',
  'divayth fyr',
  'yagrum bagarn',
}

local actorMap = {}
for _, id in ipairs(actorList) do
  actorMap[id] = true
end

return {
  engineHandlers = { 
    onActorActive = function(actor)
        if actorMap[actor.recordId] then
          actor:addScript('scripts/DramaticEntry/actors.lua')
        end
    end,
  },
  eventHandlers = {
    [events.inactive] = function(event)
      event.actor:removeScript('scripts/DramaticEntry/actors.lua')
      for _, actor in ipairs(world.activeActors) do
        if actor.type == types.Player then
          actor:sendEvent(events.inactive, {
            actor = event.actor,
          })
        end
      end
    end,
  },
}