return {
    active = 'urm_DramaticEntry_active',
    inactive = 'urm_DramaticEntry_inactive',
    animateStart = 'urm_DramaticEntry_animateStart',
    animateEnd = 'urm_DramaticEntry_animateEnd',
}