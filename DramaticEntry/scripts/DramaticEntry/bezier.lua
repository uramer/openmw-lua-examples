local cubic = function(points)
    if #points ~= 4 then
        error('Cubic bezier curve requires 4 points')
    end
    local a, b, c, d = unpack(points)

    return function(t)
        local t_2 = t * t
        local t1 = 1 - t
        local t1_2 = t1 * t1
        local value = (a * t1_2 * t1 + b * 3 * t1_2 * t + c * 3 * t1 * t_2 + d * t_2 * t)
        local tangent = (b - a) * 3 * t1 * t1 + (c - b) * 6 * t1 * t + (d - c) * 3 * t_2
        return value, tangent
    end
end

local quadratic = function(points)
    if #points ~= 5 then
        error('Quadratic bezier curve requires 5 points')
    end
    local a, b, c, d, e = unpack(points)
    local cubic1 = cubic({ a, b, c, d })
    local cubic2 = cubic({ b, c, d, e })
    return function(t)
        local value1, tangent1 = cubic1(t)
        local value2, tangent2 = cubic2(t)
        local value = value1 * (1 - t) + value2 * t
        local tangent = tangent1 * (1 - t) - value1 +  tangent2 * t + value2
        return value, tangent
    end
end

return {
    cubic = cubic,
    quadratic = quadratic,
}