local self = require('openmw.self')
local core = require('openmw.core')
local nearby = require('openmw.nearby')
local types = require('openmw.types')

local events = require('scripts.DramaticEntry.events')

local function notifyPlayers()
    for _, actor in ipairs(nearby.actors) do
        if actor.type == types.Player and actor:isValid() then
            actor:sendEvent(events.active, {
                actor = self.object
            })
        end
    end
end

if self:isActive() then
    notifyPlayers()
end

return {
    engineHandlers = {
        onActive = notifyPlayers,
        onInactive = function()
            core.sendGlobalEvent(events.inactive, {
                actor = self.object,
            })
        end,
    },
    eventHandlers = {
        [events.animateStart] = function()
            self:enableAI(false)
        end,
        [events.animateEnd] = function()
            self:enableAI(true)
        end,
    },
}