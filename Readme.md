Various simple mods and testing scripts for OpenmW Lua [Documentation](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/) [OpenMW dev build download](https://gitlab.com/OpenMW/openmw/-/pipelines)

1. **Clock** Displays the current in-game time (press `O` to toggle)
1. **Travel Distance** Measures the distance you've walked, and allows to save waypoints.  
  Very handy for road builders! (Press `,` to start/stop the measurement, `/` to save a waypoint)  
  https://www.youtube.com/watch?v=GxFTWH_6Ezg
1. **Quick Thief Tools** Quickly switch to and cycle through your lockpicks and probes. (Press `V` for lockpicks, `B` for probes)  
  https://youtu.be/IlTqNM6prh8
1. **Cinematic Camera** Smooths your camera movement. Mostly meant for recording showcases. (Press `C` to toggle).   
  https://youtu.be/55sgFwYkOUA
1. **Dramatic entry** Meet important characters in a suitably significant fashion.  
  https://youtu.be/b0_n1Bm8VCk
  