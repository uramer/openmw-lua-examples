local input = require('openmw.input')
local self = require('openmw.self')
local core = require('openmw.core')
local async = require('openmw.async')
local types = require('openmw.types')

local renderItemChoice = require('scripts.QuickThiefTools.renderChoice').renderItemChoice

local LOCKPICK_HOTKEY = input.KEY.Home
local PROBE_HOTKEY = input.KEY.End
local DISPLAY_DURATION = 2

local currentLockpick = nil
local currentProbe = nil

local function indexOf(obj, list)
    for i, o in ipairs(list) do
        if obj == o then
            return i
        end
    end
    return nil
end

local function isEquipped(tool)
    local equipment = types.Actor.equipment(self)
    return equipment[types.Actor.EQUIPMENT_SLOT.CarriedRight] == tool
end

local function equip(data)
    local equipment = types.Actor.equipment(self)
    equipment[types.Actor.EQUIPMENT_SLOT.CarriedRight] = data
    types.Actor.setEquipment(self, equipment)
end

local function cycle(list, current)
    local index = current and indexOf(current, list)
    if #list == 0 then
        current = nil
    elseif
        not index
        or isEquipped(current)
    then
        if not index or index > #list then
            index = 1
        else
            index = index + 1
            if index > #list then
                index = 1
            end
        end
        current = list[index]
    end
    if current then
        equip(current)
    end
    return current
end

local function ratioSelect(list, ratio)
    local current = nil
    if #list > 0 then
        local index = 1 + math.floor((#list - 1) * ratio + 0.5)
        current = list[index]
    end
    if current then
        equip(current)
    end
    return current
end

local element = nil
local lastUpdate = core.getSimulationTime()

local function clearDisplay()
    if core.getSimulationTime() - lastUpdate < DISPLAY_DURATION then return end
    if element then
        element:destroy()
        element = nil
    end
end

local function displayChoice(list, current)
    if element then
        element:destroy()
        element = nil
    end
    if current == nil then return end
    lastUpdate = core.getSimulationTime()
    element = renderItemChoice(list, current)
    async:newUnsavableSimulationTimer(DISPLAY_DURATION, clearDisplay)
end

local function handleTouchpadEvent(e)
    if e.position.x < 0.5 then
        local list = types.Actor.inventory(self):getAll(types.Probe)
        currentProbe = ratioSelect(list, e.position.y)
        displayChoice(list, currentProbe)
    else
        local list = types.Actor.inventory(self):getAll(types.Lockpick)
        currentLockpick = ratioSelect(list, e.position.y)
        displayChoice(list, currentLockpick)
    end
end

return {
    engineHandlers = {
        onKeyPress = function(key)
            if key.code == LOCKPICK_HOTKEY then
                local list = types.Actor.inventory(self):getAll(types.Lockpick)
                currentLockpick = cycle(list, currentLockpick)
                displayChoice(list, currentLockpick)
            elseif key.code == PROBE_HOTKEY then
                local list = types.Actor.inventory(self):getAll(types.Probe)
                currentProbe = cycle(list, currentProbe)
                displayChoice(list, currentProbe)
            end
        end,
        onTouchMove = handleTouchpadEvent,
        onTouchPress = handleTouchpadEvent,
    }
}