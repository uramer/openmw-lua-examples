local ui = require('openmw.ui')
local I = require('openmw.interfaces')

local v2 = require('openmw.util').vector2

local function renderItem(item)
    return {
        type = ui.TYPE.Container,
        content = ui.content {
            {
                template = I.MWUI.templates.padding,
                content = ui.content {
                    {
                        type = ui.TYPE.Text,
                        template = I.MWUI.templates.textNormal,
                        props = {
                            text = item.recordId,
                        },
                    },
                },
            },
        },
    }
end

local function renderItemChoice(itemList, currentItem)
    local content = {}
    for _, item in ipairs(itemList) do
        local itemLayout = renderItem(item)
        if item == currentItem then
            itemLayout.template = I.MWUI.templates.box
        else
            itemLayout.template = I.MWUI.templates.padding
        end
        table.insert(content, itemLayout)
    end
    return ui.create {
        layer = 'HUD',
        template = I.MWUI.templates.boxTransparent,
        props = {
            relativePosition = v2(0.65, 0.8),
            anchor = v2(1, 1),
        },
        content = ui.content {
            {
                type = ui.TYPE.Flex,
                content = ui.content(content),
            },
        },
    }
end

return {
    renderItemChoice = renderItemChoice
}