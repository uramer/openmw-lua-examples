local storage = require('openmw.storage')

require('scripts.CinematicCamera.settings')

local controlsSettings = storage.playerSection('SettingsCinematicCameraControls')

local MODES = {
    Off = 'off',
    Free = 'free',
    FirstPerson = 'firstPerson',
}

local modeHotkeys = {
    freeHotkey = MODES.Free,
    firstPersonHotkey = MODES.FirstPerson,
}

local modeModules = {
    [MODES.Off] = {
        on = function() end,
        update = function() end,
        off = function() end,
    },
    [MODES.Free] = require('scripts.CinematicCamera.free'),
    [MODES.FirstPerson] = require('scripts.CinematicCamera.firstPerson'),
}

local currentMode = MODES.Off

local function toggleMode(mode)
    if mode == currentMode then
        currentMode = MODES.Off
    else
        currentMode = mode
    end
end

return {
    engineHandlers = {
        onKeyPress = function(key)
            local newMode = nil
            for setting, mode in pairs(modeHotkeys) do
                if controlsSettings:get(setting) == key.code then
                    newMode = mode
                    break
                end
            end
            if not newMode then return end

            local prevMode = currentMode
            modeModules[prevMode].off()
            toggleMode(newMode)
            modeModules[currentMode].on()
        end,
        onFrame = function(dt)
            modeModules[currentMode].update(dt)
        end,
    }
}
