local core = require('openmw.core')
local input = require('openmw.input')
local ui = require('openmw.ui')
local self = require('openmw.self')

local HOTKEY = input.KEY.F8

return {
    engineHandlers = {
        onKeyPress = function(key)
            if key.code == HOTKEY then
                core.sendGlobalEvent('urm_LoadExteriors_toggle', {
                    player = self.object
                })
            end
        end,
    },
    eventHandlers = {
        ['urm_LoadExteriors_toggle'] = function(e)
            ui.showMessage(table.concat {
                'Toggled loading exteriors ', e.status and 'on' or 'off'
            })
        end,
        ['urm_LoadExteriors_progress'] = function(e)
            ui.showMessage(table.concat {
                'Loaded ', e.percent, '% of exteriors'
            })
        end,
    },
}