local util = require('openmw.util')

local time = require('openmw_aux.time')

local START = util.vector3(-20, -10, 0)
local FINISH = util.vector3(30, 10, 0)
local TELEPORT_PERIOD = 2 * time.second

local STEP = 1

local player = nil
local current = START

local function cellPosition(cell)
    return cell * 8192
end

local timer = nil

local function step()
    if not player then return end
    player:teleport('', cellPosition(current))
    local distance = current - START
    local totalDistance = FINISH - START
    local progress = (distance.y + distance.x * totalDistance.y) /
        (totalDistance.x * totalDistance.y)
    player:sendEvent('urm_LoadExteriors_progress', {
        percent = math.floor(100 * progress),
    })
    current = current + util.vector3(0, STEP, 0)
    if current.y > FINISH.y then
        current = util.vector3(current.x + STEP, START.y, START.z)
    end
    if current.x > FINISH.x then
        current = START
        if timer then timer() end
    end
end

return {
    engineHandlers = {
        onSave = function()
            return {
                current = current
            }
        end,
        onLoad = function(t)
            if t then
                current = t.current
                print('loaded current cell', current)
            end
        end,
    },
    eventHandlers = {
        ['urm_LoadExteriors_toggle'] = function(e)
            player = e.player
            if timer then
                timer()
                timer = nil
            else
                timer = time.runRepeatedly(step, TELEPORT_PERIOD)
            end
            player:sendEvent('urm_LoadExteriors_toggle', {
                status = timer ~= nil
            })
        end,
    },
}
